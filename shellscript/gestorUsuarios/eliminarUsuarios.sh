#!/bin/bash

#Este script es solo de ayuda para el de gestorUsuarios.sh y no ha sido sometido a pruebas de errores. Colocar los datos correctamente para evitar tener que hacerlo manualmente luego. El funcionamiento es el mismo que el de gestorUsuarios.sh
listado=$1
while read linea; do
	usuario=$(echo $linea | cut -d ';' -f1) 
	password=$(echo $linea | cut -d ';' -f2)
	grupo=$(echo $linea | cut -d ';' -f3)
	home=$(echo $linea | cut -d ';' -f4)
	echo $usuario, $password, $grupo
	userdel "$usuario"
	groupdel -f "$grupo"
	#Si home está vacío se eliminará el home por defecto
	if [ -z $home ];then
		rm -r /home/$usuario
	else
		rm -r $home
	fi

done<$listado