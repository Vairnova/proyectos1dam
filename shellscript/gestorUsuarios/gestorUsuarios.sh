#!/bin/bash

ERROR_NUMERO_DE_PARAMETROS_INCORRECTO=1
ERROR_EL_ARCHIVO_NO_EXISTE=2
ERROR_ESPACIOS_ENCONTRADOS=3
FORMATO_DE_RUTAS_HOME_INCORRECTO=4
FORMATO_DE_RUTAS_SHELL_INCORRECTO=5

if  [ $# -ne 1 ];then
	echo Error introduce el nombre del listado como parámetro, para ver la ayuda escribe --help como parámetro
	echo $ERROR_NUMERO_DE_PARAMETROS_INCORRECTO 1>&2
	exit
fi
if [ $1 == '--help' ];then
	echo -e "\nAYUDA DE PROGRAMA"
	echo -e "____________________________\n"
	echo Introduce el nombre del programa seguido de un parámetro.
	echo El parámetro puede ser el nombre de un archivo con un listado o --help para desplegar esta ayuda.
	echo " "
	echo La estructura del listado debe ser la siguiente:
	echo " "
	echo "usuario;contraseña;grupo;home;shell"
	echo -e "\nLos valores de home y shell no son obligatorios y se tomarán los valores por defecto si se dejan vacíos tal que así:usuario;contraseña;grupo;; o sin los delimitadores--> usuario;contraseña;grupo."
	exit 
fi

listado=$1
#Comprobación de si el listado existe y posterior lectura línea a línea del mismo seleccionando campos delimitados con ;
if [ -f $listado ]; then
	while read linea; do 
		usuario=$(echo $linea | cut -d ';' -f1) 
		password=$(echo $linea | cut -d ';' -f2)
		grupo=$(echo $linea | cut -d ';' -f3)
		home=$(echo $linea | cut -d ';' -f4)
		shell=$(echo $linea | cut -d ';' -f5)

		#Comprobaciones de errores 
		if [[ $grupo == *' '* ]];then
			echo Error. El nombre de grupo $grupo contiene espacios.
			echo $ERROR_ESPACIOS_ENCONTRADOS 1>&2
			exit
		fi
		if [[ $usuario  == *' '* ]];then
			echo Error. El nombre de usuario $usuario contiene espacios.
			echo $ERROR_ESPACIOS_ENCONTRADOS 1>&2
			exit
		fi
		if [ -z $home  ]; then
			echo home de $usuario está vacío utilizando home por defecto "/home/$usuario"
			home="/home/$usuario"
		fi
		if [[ $home != /* ]];then
			echo formato de ruta especificado como $home no utiliza un formato correcto, debe empezar con /
			echo $FORMATO_DE_RUTAS_HOME_INCORRECTO 1>&2
			exit
		fi
		#El siguiente if comprueba si se ha escrito algo en la parte de shell par añadir el por defecto en caso negativo.
		if [ -z $shell  ]; then
			echo shell de $usuario está vacío utilizando shell /bin/bash
			shell="/bin/bash"
		fi
		#Este for se utiliza para comprobar que si el usuario ha introducido un shell manualmente, que coincida con uno de los shell existentes en /etc/shells
		for fichero in "/etc/shells";do
			if [[ -z $( grep $shell $fichero)  ]];then
				echo formato shell especificado como $shell no está disponible debe utilizar uno de los shell disponibles:
				cat $fichero
				echo $FORMATO_DE_RUTAS_SHELL_INCORRECTO 1>&2
				exit
			fi
		done
		#Se muestra la confirmación de datos y lo que se va crear previamente.
		echo Formato de datos del usuario $usuario correcto.
		echo Creando el usuario según lo especificado.
		echo $usuario, $password, $grupo, $home, $shell

		#Se crean los distintos usuarios con los parámetros especificados.
		groupadd -f "$grupo"
		useradd -g $grupo -d $home -m -s $shell "$usuario"

		echo "$password" >> archivoTemporalPasswd
		echo "$password" >> archivoTemporalPasswd
		passwd "$usuario"<archivoTemporalPasswd
		rm archivoTemporalPasswd

		echo -e "_________________________________________________ \n"                 

	done<$listado
	echo -e "\nFINALIZADO \n"

else
	echo no existe o no es un fichero de texto
	echo $ERROR_EL_ARCHIVO_NO_EXISTE 1>&2
	exit
fi