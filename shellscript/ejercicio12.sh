#!/bin/bash
ERROR_NUMERO_DE_PARAMETROS_INCORRECTO=1
ERROR_USUARIO_NO_VALIDO=2

if [ $# -ne 1 ];then  #Dos if para control de ejecución del programa
	echo $ERROR_NUMERO_DE_PARAMETROS_INCORRECTO 1>&2
	echo ERROR se debe introducir un nombre de usuario como parámetro ./ejercicio12 nombreDeUsuario
	exit
fi
usuario=$1
if [ -z $(grep $usuario /etc/passwd) ]; then
	echo $ERROR_USUARIO_NO_VALIDO 1>&2
	echo Error el nombre introducido no corresponde a un usuario o no existe.
	exit
fi

#Dos variables con las que usar la fecha y la hora más cómodamente.
fecha=$(date +%d%m%Y)
hora=$(date +%H%M)

echo Copiando archivos del directorio del usuario $usuario...

#por si no existe el directorio
mkdir -p /home/$usuario/copias/ficheros 
#copia de todo lo del usuario en la ubicación designada
#cp -r /home/$usuario/hola/ /home/$usuario/copias/ficheros/seguridad\_$usuario\_Fecha\_$fecha\_Hora\_$hora

#comprimimos todo y le ponemos el nombre usando las variables adecuadas.
tar -cvzf  /home/$usuario/copias/ficheros/seguridad\_$usuario\_Fecha\_$fecha\_Hora\_$hora.tar.gz /home/$usuario
# por si no existe el directorio
mkdir -p ~/copias/logs 
# hace una pasada por todos los archivos y guarda su nombre el otro archivo indicando usuario, fecha y hora.
for archivo in /home/$usuario; do 
	echo $(ls $archivo) >>~/copias/logs/copia\_$usuario\_Fecha\_$fecha\_Hora\_$hora.txt
done
