#!/bin/bash
ERROR_NUMERO_DE_PARAMETROS_INCORRECTO=1
ERROR_PARAMETRO_DESCONOCIDO=2

if [ $# -ne 2 ];then     #Comprobación de que se han introducido los parámetros necesarios.
	echo $ERROR_NUMERO_DE_PARAMETROS_INCORRECTO 1>&2
	echo "ERROR introduce dos parámetros. Usar así: ./ejercicio11.sh IE(IE:Ingles-Español/EI:Español-Inglés) palabraATraducir"
	exit
fi
if [[ $1 != "EI" && $1 != "IE" ]];then #comprobación de que se ha introducido lo que pide para traducir luego según el parámetro.
	echo $ERROR_PARAMETRO_DESCONOCIDO
	echo "ERROR especifica EI para traducir de español a inglés o IE para traducir de inglés a español Ejemplo: ./ejercicio11.sh EI palabra"
	exit
fi

estar=$(grep -w $2 <diccionario.txt) #Variable para buscar si la palabra está en el diccionario
if [ -z $estar ];then ##Si la palabra no está en el diccionario...
	echo "La palabra $2 no está en el diccionario ¿deseas añadirla? (si/no)"
	read respuesta 
	if [[ ($respuesta == "si" || $respuesta == "SI" || $respuesta == "Si") && $1 == "EI" ]];then #...y el usuario responde que quiere añadir la palabra...(suponemos que la palabra que ha introducido está en español al querer traducir al inglés y por eso hacemos este if)
		echo "¿Qué significa en inglés?"
		read traduccion
		echo "$2;$traduccion">>diccionario.txt #... la pide por teclado y la añade al final del diccionario.

		#Pequeña chapuza hecha para que al no poner simplemente sort diccionario.txt>diccionario.txt no lo deje en blanco sino que actualize el diccionario ordenado con la palabra nueva
		sort diccionario.txt > diccionarioOrdenado.txt 
		cat diccionarioOrdenado.txt > diccionario.txt
		rm -r diccionarioOrdenado.txt
	elif [[ ($respuesta == "si" || $respuesta == "SI" || $respuesta == "Si") && $1 == "IE" ]];then ##...y el usuario responde que quiere añadir la palabra...(suponemos que la palabra que ha introducido está en inglés al querer traducir al español y por eso hacemos este elif)
		echo "¿Qué significa en español?"
		read traduccion
		echo "$traduccion;$2">>diccionario.txt #... la pide y la coloca al final del diccionario

		#pequeña chapuza parte 2 para ordenar el diccionario
		sort diccionario.txt > diccionarioOrdenado.txt 
		cat diccionarioOrdenado.txt > diccionario.txt
		rm -r diccionarioOrdenado.txt
	fi

else #Si la palabra esta en el diccionario...
	if [ $1 == "EI" ]; then #... y el usuario ha pasado por parámetro que quiere traducir de español a inglés...
		traduccion=$(echo $(grep -w $2 | cut -d ";" -f2)) #busca la posición de la palabra en el diccionario y enseña la segunda columna que es la que contiene la traducción.
		echo $2 es $traduccion en inglés
	fi<diccionario.txt 

	if [ $1 == "IE" ]; then #... y el usuario ha pasado por parametro que quiere traducir de inglés a español...
		traduccion=$(echo $(grep -w $2 | cut -d ";" -f1)) #busca la posición de la palabra en el diccionario y enseña la primera columna que es la que contiene la traducción.
		echo $2 es $traduccion en español
	fi<diccionario.txt 
fi
