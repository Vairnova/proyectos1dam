#!/bin/bash
echo -e "Selecciona lo que quieres hacer: \n"  #Todas las opciones que se pueden realizar mostradas por pantalla
echo "añadir (añadir contacto)"
echo "listar (visualizar toda la agenda)"
echo "buscar (buscar entradas por nombre, dirección o teléfono)"
echo "ordenar (ordenar los registros alfabéticamente)"
echo "borrar (muestra una lista con los nombres de usuario y borrar la línea del que se indique)"
echo "salir(Cerrar el programa)"
read eleccion
#while que controla el switch y no dejará salir hasta que reciba la palabra "salir"
while [[ $eleccion != "salir" && $eleccion != "SALIR" ]]; do 

	#Lee las distintas variables y las escribe en agenda al final del documento
	case $eleccion in 
	añadir)	
			echo introduce el nombre del nuevo contacto
			read nombre
			echo introduce su dirección
			read direccion
			echo introduce su teléfono
			read telefono

			echo "$nombre;$direccion;$telefono" >> agenda.txt

	;;

	#While que lee todas las líneas del fichero agenda y en cada línea separa en tres campos diferenciados el nombre-campo1 direccion-campo2 telefondo-campo3
	listar)  
	
		while read linea; do
			nombre=$(echo $linea | cut -d ";" -f1)
			direccion=$(echo $linea | cut -d ";" -f2)
			telefono=$(echo $linea | cut -d ";" -f3)

			echo "Nombre: $nombre ||" "Dirección: $direccion ||" "Teléfono: $telefono"
			echo ---------------------------
		done < agenda.txt
	;;

	#buscará mediante grep -w palabras completas que coincidan con la introducida y mostrará las líneas en las que aparezca (tanto si aparece en nombre como en dirección)
	buscar) 
	
		echo introduce una palabra para buscar
		read palabra
		agrupacion=$(echo $(grep -w $palabra agenda.txt))
		for palabra in $agrupacion;do
			echo "Nombre:"
			echo  $palabra | cut -d ";" -f1
			echo "Dirección:"
			echo  $palabra | cut -d ";" -f2
			echo "Teléfono:"
			echo  $palabra  | cut -d ";" -f3
		done 

	;;
	#ordenar registros alfabéticamente
	ordenar) 
		sort agenda.txt > agenda2.txt
		mv agenda2.txt agenda.txt
		cat agenda.txt
	;;

	borrar)
		cat -b agenda.txt # Mostramos todo y preguntamos 
		echo "¿A quién quieres borrar?(Elige el número de línea)"
		read numero
		cont=0
		while read linea; do
			cont=$(($cont+1))
			if [ $cont -ne $numero ];then
				echo $linea>>agendaBorrada.txt
			fi

		done<agenda.txt
		cat agendaBorrada.txt>agenda.txt
		rm -r agendaBorrada.txt

	;;
	*)
		#opción por defecto por si se ha escrito algo que no aparezca en el case
		echo ERROR elige una de las opciones dadas  
		;;
	esac

echo -e "\n¿Qué quieres hacer ahora? \n" #Volvemos a enseñar las opciones al final para que el usuario sepa que hacer cuando el cursos se queda parpadeando esperando una opción (En programación se arreglaría con un do while pero aquí no sé :( )
echo "añadir (añadir contacto)"
echo "listar (visualizar toda la agenda)"
echo "buscar (buscar entradas por nombre, dirección o teléfono)"
echo "ordenar (ordenar los registros alfabéticamente)"
echo "borrar (muestra una lista con los nombres de usuario y borrar la línea del que se indique)"
echo "salir(Cerrar el programa)"
read eleccion
done

