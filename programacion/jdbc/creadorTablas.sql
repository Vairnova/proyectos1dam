CREATE TABLE dinos
(
	nom_cientifico VARCHAR(50)  PRIMARY KEY,
	nom_comun VARCHAR(40) UNIQUE NOT NULL,
	tipo_dino VARCHAR(30) NOT NULL,
	habitat VARCHAR(30)  REFERENCES ecosistema(tipo) ON DELETE CASCADE, 
	alimentacion VARCHAR(30) NOT NULL,
	temperamento VARCHAR(25)
);

CREATE TABLE ecosistema
(
	tipo VARCHAR(30)  PRIMARY KEY,
	humedad int(3) NOT NULL
);

