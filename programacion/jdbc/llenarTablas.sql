INSERT INTO dinos
VALUES ('tyrannosaurus rex', 'rex', 'terrestre', 'llanuras','carnivoro', 'agresivo');
INSERT INTO dinos
VALUES ('utharaptor prime', 'velociraptor', 'terrestre', 'jungla','carnivoro', 'agresivo');
INSERT INTO dinos
VALUES ('brontosaurus lazarus', 'bronto', 'terrestre', 'llanuras','herbivoro', 'docil');
INSERT INTO dinos
VALUES ('dilophosaurus sputatrix', 'dilo', 'terrestre', 'jungla','carnivoro', 'agresivo');
INSERT INTO dinos
VALUES ('pachycephalosaurus leniproelia', 'pachy', 'terrestre', 'llanuras','herbivoro', 'pasivo');
INSERT INTO dinos
VALUES ('argentavis atrocollum', 'argentavis', 'aereo', 'montañas','carroñero', 'agresivo');
INSERT INTO dinos
VALUES ('parasaurolophus amphibio', 'parasaurio', 'terrestre', 'llanuras','herbivoro', 'huidizo');
INSERT INTO dinos
VALUES ('triceratops styrax', 'trike', 'terrestre', 'llanuras','herbivoro', 'defensivo');
INSERT INTO dinos
VALUES ('pteranodon wyvernus', 'pteranodon', 'aereo', 'llanuras','carnivoro', 'huidizo');
INSERT INTO dinos
VALUES ('quetzalcoatlus conchapicern', 'quetzal', 'aereo', 'montañas','carnivoro', 'huidizo');
INSERT INTO dinos
VALUES ('carchodon ultramegalodon', 'megalodon', 'acuatico', 'oceano','carnivoro', 'agresivo');
INSERT INTO dinos
VALUES ('ichtyosaurus curiosa', 'ichty', 'acuatico', 'oceano','carnivoro', 'curioso');
INSERT INTO dinos
VALUES ('mosasaurus suspirita', 'mosasaurus', 'acuatico', 'oceano','carnivoro', 'agresivo');
INSERT INTO dinos
VALUES ('paraceratherium gigamicus', 'paracer', 'terrestre', 'desierto','herbivoro', 'docil');

INSERT INTO ecosistema
VALUES ('llanuras', '50');
INSERT INTO ecosistema
VALUES ('jungla', '90');
INSERT INTO ecosistema
VALUES ('montañas', '20');
INSERT INTO ecosistema
VALUES ('oceano', '100');
INSERT INTO ecosistema
VALUES ('desierto', '3');