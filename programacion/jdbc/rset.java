import java.sql.*;
public class rset
{
	 public static void main(String args[])
	{
		Connection conexion = null;
        Statement stmt = null;
        ResultSet rs = null;
        try 
        {
           
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            System.out.println("Driver "+driver+" Registrado correctamente");
            
            System.out.println("Conectando con la Base de datos...");
            String jdbcUrl = "jdbc:mysql://localhost:3306/dinosaurios";
            conexion = DriverManager.getConnection(jdbcUrl,"root","");
            System.out.println("Conexión establecida con la Base de datos...");

            stmt = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
            String sql;
           
            sql = "SELECT * FROM dinos";
            rs = stmt.executeQuery(sql);
			String nomCientifico,nomComun,tipo,habitat,alimentacion,temperamento;
			System.out.println("Se permite modificar el nombre común de alguna de las criaturas, ¿cuál quieres modificar?(introducir nom_comun.)(Intro para ignorar esto)");
			String cambiar=System.console().readLine();
			
		 	while(rs.next())
            { 
            	nomCientifico = rs.getString("nom_cientifico");
				nomComun = rs.getString(2);
				tipo=rs.getString(3);
				habitat=rs.getString(4);
				alimentacion=rs.getString(5);
				temperamento=rs.getString(6);
            	System.out.println("Nombre científico: " + nomCientifico + " Nombre común: " + nomComun+" Tipo: "+ tipo +" Habitat: "+habitat+" Alimentación: "+alimentacion+" Temperamento: "+temperamento+"\n");
		        if (nomComun.equals(cambiar))
				{
					System.out.println("Encontrado el nombre especificado, ¿cuál deseas introducir en su lugar?");
					String nuevoNombre=System.console().readLine();
					rs.updateString("nom_comun",nuevoNombre);
					rs.updateRow();
					System.out.println("Modificado");

				}
		    }

		}
	    
        catch(SQLException se) 
        {
            //Errores de JDBC
            se.printStackTrace();
        } 
        catch(Exception e) 
        {
            //Errores de Class.forName
            e.printStackTrace();
        } 
        finally
        {
            try 
            {
            	if (rs != null)
            		rs.close();
                if(stmt!=null)
                    stmt.close();                
                if(conexion!=null)
                    conexion.close();
                System.out.println("Conexión cerrada");
            } 
            catch(SQLException se) 
            {
                se.printStackTrace();
        	}
        }
    }
	   
	
}