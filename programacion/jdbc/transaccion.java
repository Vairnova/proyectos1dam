import java.sql.*;
public class transaccion
{
	 public static void main(String args[])
	{
		Connection conn = null;
        Statement stmt = null;
        try 
        {
           
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
		    System.out.println("Driver "+driver+" Registrado correctamente");

		  
		    System.out.println("Conectando con la Base de datos...");
		    String jdbcUrl = "jdbc:mysql://localhost/dinosaurios";
		    conn = DriverManager.getConnection(jdbcUrl,"root","");
		    System.out.println("Conexión establecida con la Base de datos...");
		    
		    stmt = conn.createStatement();

		    conn.setAutoCommit(false);
			String sql= "UPDATE ecosistema SET humedad = humedad -5 WHERE tipo = 'jungla'";
			stmt.executeUpdate(sql);
			sql= "UPDATE ecosistema SET humedad = humedad +5 WHERE tipo ='desierto'";
			stmt.executeUpdate(sql);
			conn.commit();
		}
		catch(SQLException se)
		{
			se.printStackTrace();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				conn.setAutoCommit(true);
				if(stmt!=null)
					stmt.close();
				if(conn!=null)
					conn.close();
			}
			catch(SQLException se)
			{
				se.printStackTrace();
			}
		}
	}
}