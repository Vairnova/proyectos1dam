//Este programa realizará las funciones de inserción, borrado y actualización según se especifique como parámetro. Deben seguirse las instrucciones: java funcionesBasicas operacion valor1 valor2 valor3 ...
import java.sql.*;
public class funcionesBasicas
{
	 public static void main(String args[])
	{
		if(args.length > 0)
		{
			if(args[0].equals("INSERT") || args[0].equals("insert"))
			{
				insertar(args);
			}
			if(args[0].equals("UPDATE") || args[0].equals("update"))
			{
				actualizar(args);
			}
			if(args[0].equals("DELETE") || args[0].equals("delete"))
			{
				eliminar(args);
			}

		}
		else
			System.out.println("Introduce lo que quieres realizar como primer parámetro (insert,update o delete)");
	}
	public static void insertar(String args[])
	{
		Connection conn=null;
		Statement stmt = null;
			try
			{
				String driver = "com.mysql.jdbc.Driver";
				Class.forName(driver).newInstance();
				System.out.println("Driver "+ driver+" registrado correctamente.");

				System.out.println("Conectando con la base de datos...");
				String jdbcUrl = "jdbc:mysql://localhost/dinosaurios";

				conn = DriverManager.getConnection(jdbcUrl,"root","");

	            System.out.println("Conexión establecida con la Base de datos...");
	            stmt = conn.createStatement();
	            if(args.length == 7)
	            {
	           		String sql = "INSERT INTO dinos VALUES ('"+ args[1] + "','" + args[2]+"','"+ args[3] + "','"+args[4]+"','"+args[5]+"','"+args[6]+"')";
	        		stmt.executeUpdate(sql);
	            
	            	System.out.println("Registro añadido!");
	            }
	            else
	        
		    		if(args.length == 3)
		    		{
		            	String sql = "INSERT INTO ecosistema VALUES ('"+ args[1] + "'," + Integer.parseInt(args[2])+")";
		        		stmt.executeUpdate(sql);
		            
		            	System.out.println("Registro añadido!");
		            }
		            else
		            	System.out.println("Número de parámetros no coincide con la tabla especificada.");
	            
	            
	        } 
	        catch(SQLException se) 
	        {
	            
	            se.printStackTrace();
	        } 
	        catch(Exception e) 
	        {
	            
	            e.printStackTrace();
	        } 
	        finally 
	        {

	            try 
	            {
	                if(stmt!=null)
	                    stmt.close();                
	                if(conn!=null)
	                    conn.close();
	                System.out.println("Conexión cerrada");
	            } 
	            catch(SQLException se) 
	            {
	                se.printStackTrace();
	            }
	        }
	}
	public static void actualizar(String args[])
	{
		Connection conn=null;
		Statement stmt = null;
		try{
				String driver = "com.mysql.jdbc.Driver";
				Class.forName(driver).newInstance();
				System.out.println("Driver "+ driver+" registrado correctamente.");

				System.out.println("Conectando con la base de datos...");
				String jdbcUrl = "jdbc:mysql://localhost/dinosaurios";

				conn = DriverManager.getConnection(jdbcUrl,"root","");
	            System.out.println("Conexión establecida con la Base de datos...");
	            stmt = conn.createStatement();
	            String tabla;

	            System.out.println("¿Qué tabla quieres actualizar?");
	            tabla=System.console().readLine();

	            if(tabla.equals("dinos"))
	            {
	            	if(args.length==7)
	            	{
	            	System.out.println("¿Qué criatura deseas modificar? (Indica nombre común)");
	            	String nombre=System.console().readLine();
	            	String sql = "UPDATE dinos SET nom_cientifico = '"+args[1]+"', nom_comun='"+args[2]+"', tipo_dino ='"+args[3]+"', habitat ='"+args[4]+"', alimentacion = '"+args[5]+"', temperamento= '"+args[6]+"' WHERE nom_comun= '"+nombre+"'";
	            		stmt.executeUpdate(sql);
	            		System.out.println("Registro actualizado");
					}
					else
						System.out.println("Debes introducir los valores como parámetro: java funcionesBasicas update valor1 valor2 ... valor6");
				}
				else
					if(tabla.equals("ecosistema"))
					{
						if(args.length==3)
	            		{
							System.out.println("¿Qué ecosistema deseas modificar? (Indica tipo)");
		            		String tipo=System.console().readLine();
							String sql = "UPDATE ecosistema SET tipo= '"+args[1]+"', humedad='"+ Integer.parseInt(args[2])+"' WHERE tipo = '"+tipo+"'";
							stmt.executeUpdate(sql);
							System.out.println("Registro actualizado");
						}
						else
							System.out.println("Introduce los valores como parámetro: java funcionesBasicas update valor1 valor2");
					}
					else
					{
						System.out.println("No existe esa tabla en esta base de datos.");
					}
	         
	        } 
	        catch(SQLException se) {
	            
	            se.printStackTrace();
	        } catch(Exception e) {
	            
	            e.printStackTrace();
	        } finally {
	            try {
	                if(stmt!=null)
	                    stmt.close();                
	                if(conn!=null)
	                    conn.close();
	                System.out.println("Conexión cerrada");
	            } catch(SQLException se) {
	                se.printStackTrace();
	            }
	        }

	}
	
	public static void eliminar(String args[])
	{
		Connection conn=null;
		Statement stmt = null;
		try{
			String driver = "com.mysql.jdbc.Driver";
			Class.forName(driver).newInstance();
			System.out.println("Driver "+ driver+" registrado correctamente.");

			System.out.println("Conectando con la base de datos...");
			String jdbcUrl = "jdbc:mysql://localhost/dinosaurios";

			conn = DriverManager.getConnection(jdbcUrl,"root","");
            System.out.println("Conexión establecida con la Base de datos...");
            stmt = conn.createStatement();
            System.out.println("¿De qué tabla quieres eliminar?");
            String tabla=System.console().readLine();
            if(tabla.equals("dinos"))
            {
            System.out.println("¿Qué criatura quieres eliminar?(especificar nombre común)");
            String nombre = System.console().readLine();
            String sql = "DELETE FROM "+tabla+" WHERE nom_comun= '" + nombre+"'";

            stmt.executeUpdate(sql);
            
            System.out.println("Registro eliminado");
            }
            else
             	if(tabla.equals("ecosistema"))
             	{
             		System.out.println("¿Que ecosistema eliminar?(especificar tipo)");
		            String nombre = System.console().readLine();
		            String sql = "DELETE FROM "+tabla+" WHERE tipo= '" + nombre+"'";
		            stmt.executeUpdate(sql);
		            System.out.println("Registro eliminado");
		        }
		        else
		        	System.out.println("No existe esa tabla");
        } catch(SQLException se) {
            
            se.printStackTrace();
        } catch(Exception e) {
            
            e.printStackTrace();
        } finally {
            try {
                if(stmt!=null)
                    stmt.close();                
                if(conn!=null)
                    conn.close();
                System.out.println("Conexión cerrada");
            } catch(SQLException se) {
                se.printStackTrace();
            }
        }
	}
	    
}
