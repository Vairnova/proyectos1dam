import java.sql.*;
public class preparedSt
{
	 public static void main(String args[])
	{
		Connection conn = null;
        PreparedStatement pstmt = null;
        
        try 
        {
           
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            
            System.out.println("Driver "+driver+" Registrado correctamente");
            
          
            System.out.println("Conectando con la Base de datos...");
            String jdbcUrl = "jdbc:mysql://localhost:3306/dinosaurios";
            conn = DriverManager.getConnection(jdbcUrl,"root","");
            System.out.println("Conexión establecida con la Base de datos...");
            
            System.out.println("¿En qué tabla deseas añadir información?");
            String tabla=System.console().readLine();

            if (tabla.equals("dinos"))
            {
	            pstmt = conn.prepareStatement("INSERT INTO dinos SET nom_cientifico=?,nom_comun=?,tipo_dino=?,habitat=?,alimentacion=?,temperamento=?");
	            System.out.println("Nombre científico de la nueva criatura");
	            String nombreCientifico=System.console().readLine();            
	            System.out.println("Nombre común de la nueva criatura");
	            String nombreComun=System.console().readLine(); 
	            System.out.println("Tipo de la nueva criatura");
	            String tipo=System.console().readLine(); 
	            System.out.println("Habitat de la nueva criatura");
	            String habitat=System.console().readLine(); 
	            System.out.println("Alimentación de la nueva criatura");
	            String alimentacion=System.console().readLine(); 
	            System.out.println("Temperamento de la nueva criatura");
	            String temperamento=System.console().readLine(); 

	            pstmt.setString(1,nombreCientifico);
	            pstmt.setString(2,nombreComun);
	            pstmt.setString(3,tipo);
		    	pstmt.setString(4,habitat);
		    	pstmt.setString(5,alimentacion);
		    	pstmt.setString(6,temperamento);

		    	pstmt.executeUpdate();
            	System.out.println("Nueva criatura añadida");
	    	}
	    	else 
	    		if (tabla.equals("ecosistema")) 
	    		{
	    			pstmt = conn.prepareStatement("INSERT INTO ecosistema SET tipo=?,humedad=?");
	    			System.out.println("Nombre del nuevo ecosistema");
	    			String tipo=System.console().readLine();
	    			System.out.println("Porcentaje de humedad del nuevo ecosistema");
	    			int humedad = Integer.parseInt(System.console().readLine());
	    			pstmt.setString(1,tipo);
	    			pstmt.setInt(2,humedad);
	    			pstmt.executeUpdate();
            		System.out.println("Nuevo ecosistema añadido");
	    		}
	    		else
	    			System.out.println("Tabla inexistente.");   
        } 
        catch(SQLException se) 
        {
            se.printStackTrace();
        }
        catch(Exception e) 
        {
            e.printStackTrace();
        } 
        finally 
        {
            try 
            {
                if(pstmt!=null)
                    pstmt.close();
                if(conn!=null)
                    conn.close();
                System.out.println("Cerrando conexión con la BD");
            } 
            catch(SQLException se) 
            {
              	se.printStackTrace();
            }
        }
	}
}