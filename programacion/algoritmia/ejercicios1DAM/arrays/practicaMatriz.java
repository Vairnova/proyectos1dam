//Programa que cargue valores en una matriz 3x4 (leídos desde teclado), muestre la matriz y gener un vector con los valores promedios de cada fila
public class practicaMatriz
{
	public static void main(String[] args) 
	{
		 final int FILAS=3, COLUMNAS=4;
		int n1;
		double n2=0;
		int matriz[][]= new int[FILAS][COLUMNAS];
		double medias[] = new double[FILAS];

		for(int f=0; f<FILAS; f++)
			for(int c=0; c<COLUMNAS; c++)
			{
				System.out.println("Introduce un valor");
				n1 = Integer.parseInt(System.console().readLine());
				matriz[f][c] = n1;
			}
			for(int f=0; f<FILAS; f++)
			{
				for(int c=0; c<COLUMNAS; c++)
				{
				System.out.print(matriz[f][c] + "\t");
				}
			System.out.println("");
			}

			for(int f=0; f<FILAS; f++)
			{
				for(int c=0; c<COLUMNAS; c++)
				{
					n2 += matriz[f][c];
				}

				medias[f] = n2 / COLUMNAS;
				System.out.println("La media de la fila " + (f+1) + " es " + medias[f]);
				n2=0;
			}
			

	}
}