/*Programa de ejemplo para búsqueda lineal booleana.
 forma de uso:
 java busquedaLinealBooleana valorBuscado 
 	ej: java busquedaLinealBooleana 9
 	*/

public class busquedaOrdenadaLinealBooleana
{

	//private final static int TAM = 5;
	public static void main (String args[])
	{
		if(args.length > 0)
		{
		int nums[] = {1,3,4,6,7,9,11,13,15,16,17,19,20,22,23,25};
		//obtengo el valor de búsqueda.
		int busca = Integer.parseInt(args[0]);

		if(busqueda(busca,nums)== true)
			System.out.println("Encontrado");
		else
			System.out.println("No ta");

		}
		else
			System.out.println("Forma de uso:  java busquedaOrdenadaLinealBooleana valorBuscado ");
	}
	public static boolean busqueda(int valor, int array[])
	{
		for(int i = 0; i < array.length;i++)
			if(valor==array[i]) // comparo el valor de esa posición en el array con el valor buscado.
			return true;
			else
				if(valor<array[i])
					return false;
		return false;//Llega hasta el final sin encontrarlo
		
	}
}