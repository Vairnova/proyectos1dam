
/* Realiza un ejercicio (preferentemente modular ) que permita almacenar nombres (personas o ciudades, ..., lo que prefieras) en un objeto Vector, con métodos que permitan cargar sus valores de teclado, mostrar todos los nombres por pantalla, etc. El objeto Vector será creado con una capacidad inicial de 10 e incrementos de 5 en 5.
 El programa guardará todos los nombres que quiera introducir el usuario hasta acabar con una línea vacía, mostrará todos los nombres almacenados, así como el tamaño y capacidad del vector, para acabar pidiendo un nombre que habrá de eliminar del Vector. */ 


import java.util.Vector;
import java.util.Scanner;

public class ejVector
{
	public static void main (String args[])
	{
		Scanner ent= new Scanner(System.in);
		Vector<String> ciudades = new Vector<String>();
		String s = "";

		do
		{
			System.out.println("Introduce una ciudad");
			s= ent.nextLine();
			if(s.equals(""))
				break;
			ciudades.add(s);
		}while(!ciudades.isEmpty());

		System.out.println(ciudades.capacity());
		
		System.out.println(ciudades.elements());




	}
}