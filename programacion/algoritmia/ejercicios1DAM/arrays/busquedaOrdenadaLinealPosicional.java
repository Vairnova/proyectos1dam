/*Programa de ejemplo para búsqueda lineal booleana.
 forma de uso:
 java busquedaLinealBooleana valorBuscado 
 	ej: java busquedaLinealBooleana 9
 	*/

public class busquedaOrdenadaLinealPosicional
{
	//private final static int TAM = 5;
	public static void main (String args[])
	{
		if(args.length > 0)
		{
			int nums[] = {1,2,4,5,7,8,10,13,15,16,19,20,24,25,27,28,30,33};
			//obtengo el valor de búsqueda.
			int busca = Integer.parseInt(args[0]);

			int resultado= busqueda(busca,nums);
			
			if(resultado >=0 )
				System.out.println("Encontrado en la posición " + (resultado +1));
			else
				System.out.println("No ta");
		}
		else
			System.out.println("Forma de uso:  java busquedaLinealPosicional valorBuscado ");
	}
	public static int busqueda(int valor, int array[])
	{
		for(int i = 0; i < array.length;i++)
		{
			if(valor==array[i]) // comparo el valor de esa posición en el array con el valor buscado.
			return i;
			else
				if(valor<array[i])
					return -1;
		}
		return -1;
		
	}
}