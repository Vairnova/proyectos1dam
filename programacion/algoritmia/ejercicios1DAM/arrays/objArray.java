//Ejemplo de creación de un array de objetos. (SOLUCIÓN MODULAR)
/*Programa que cree un array de 5 objetos rectángulo, con acho y alto elegido por el usuario, y muestre el área de
todos ellos */
import java.util.Scanner;


public class objArray
{
    static final int TAM = 5;

    public static void cargaArray(rectangulo rs[])
    {
        
        for (int i=0; i<TAM; i++)
        {
            double an, al;
            Scanner ent = new Scanner(System.in);

            System.out.println("Introduce el ancho del rectángulo " + (i +1) + ": ");
            an = ent.nextDouble();
            System.out.println("Introduce el ancho del rectángulo: " + (i +1) + ": ");
            al = ent.nextDouble();
            
            rs[i]= new rectangulo(an, al); //creo cada rectángulo individualmente
            
        }
    }
    public static void muestraAreas(rectangulo rt[])
    {
        for (int i=0; i<TAM; i++)
        {
        System.out.println("El área del rectángulo "+ (i+1) + " es :" + rt[i].area());
        }
    }
    public static void main(String args[])
    {
        
        
        rectangulo r[] = new rectangulo[TAM];
        
        cargaArray(r);  //LLamo a los métodos anteriores pasando como parámetro el array
        muestraAreas(r);
            
        //Falta mostrar el área para cada rectángulo
        
    }
}
