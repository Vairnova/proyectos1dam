public class mainOrdenarBuscar
{
	public static void main(String args[])
	{
		int num;

		ordenarBuscar ord1 = new ordenarBuscar();
		int nums[] = new int[100];

		nums= ord1.generaAleatorios();
		ord1.mostrarNumeros(nums);

		System.out.println("Introduce un número entre 1 y 100");
		num= Integer.parseInt(System.console().readLine());

		boolean estar = ord1.buscarDesordenado(nums, num);
		if(estar)
			System.out.println("El valor está en el array");
		else 
			System.out.println("El valor no está en el array");

		nums=ord1.ordenarBurbujaOptimo(nums);
		ord1.mostrarNumeros(nums);

		System.out.println("Introduce un segundo número a buscar: ");
		num= Integer.parseInt(System.console().readLine());
		int pos=ord1.buscarDicotomica(nums, num );
		if(pos<0)
			System.out.println("Número no encontrado");
		else
			System.out.println("Número encontrado");

		pos= ord1.buscarLineal(nums, num);
		if(pos==0)
			System.out.println(";No se encuentra en el array");
		else
			System.out.println("Se ha encontrado un total de " + pos + " veces.");

	}
}