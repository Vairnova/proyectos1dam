/*a) Realiza un programa en Java que haga lo siguiente (solución preferentemente modular, todo en una misma clase que incluya main y el resto de funciones):

    - genere 100 números enteros aleatorios, entre 1 y 100, y los cargue en un array de 100 enteros
    - muestre el array por pantalla
    - pida al usuario un valor (entre 1 y 100 también) e indique si, o no, se encuentra en el array (búsqueda lineal en vector desordenado)
    - ordene el array con el método de la burbuja
    - vuelva a mostrar el array
    - pida otro valor, entre 1 y 100, al usuario
    - realice otra búsqueda (ahora dicotómica) indicando, igualmente, si se encuentra, o no, en el array
    - con el mismo valor de búsqueda, finalmente realizará una búsqueda lineal indicando cuántas veces ha aparecido el valor en el array
   
b) Realiza el mismo ejercicio con 2 clases: una primera clase para el programa, y otra clase instanciable que incluya como atributo el array y como métodos los correspondientes a las operaciones necesarias (cargar el array, mostrar por pantalla, ordenar y las distintas búsquedas ...).*/

public class ordenarBuscar
{
	private int numeros[]; 

	public int[] generaAleatorios()
	{
		int nums[] = new int[100];
		for(int i = 0; i< nums.length; i++)
			nums[i]= (int)(1+100*Math.random());
		return nums;
	}
	public void mostrarNumeros(int nums[])
	{
		for(int i=0;i<nums.length;i++)
			System.out.println(nums[i]);
	}
	public boolean buscarDesordenado(int nums[],int num)
	{
		for(int i=0;i<nums.length;i++)
		{
			if(nums[i]== num)
				return true;
			
		}
		return false;
	}
	public int[] ordenarBurbujaOptimo(int nums[])
	{
		int aux;
		boolean ordenado= false;
		for(int tope= nums.length -2; tope>=0 && (!ordenado); tope--)
		{
			ordenado=true;
			for(int i=0; i<=tope ;  i++)
				if (nums[i] > nums[i+1])
				{
					aux= nums[i];
					nums[i]= nums[i+1];
					nums[i+1]= aux;
					ordenado=false;
				}		
		}
		return nums;
	}

	public int buscarDicotomica( int nums[], int num)
	{
		int izq, der, centro;
		izq=0;
		der=nums.length-1;
		centro=(izq+der)/2;
		while((izq < der) && (nums[centro] != num))
		{
			if(nums[centro]>num)
				der=centro-1;
			else
				izq=centro+1;
			centro=(izq+der)/2;
		}

		if(nums[centro] == num)
			return centro;
		return -1;
	}
	public int buscarLineal(int nums[], int num)
	{
		int cont=0;
		for(int i=0; i<nums.length;i++)
		{
			if (nums[i]== num)
				cont++;
		}
		return cont;
	}

}