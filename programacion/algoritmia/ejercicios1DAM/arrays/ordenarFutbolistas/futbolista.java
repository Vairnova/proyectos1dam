public class futbolista
{
	private String nom;
	private int goles;

	public futbolista(){nom="Messi"; goles=1;}
	public futbolista(String n, int g)
	{
		nom=n; goles = g;
	}
	public futbolista(futbolista f){ nom = f.nom; goles = f.goles;}

	//setters ... y getters

	public void anotaGol() { goles++;}
	public int getGoles(){return goles;}
	public String toString() { return "Nombre: " + nom + ", goles: " + goles; }

}