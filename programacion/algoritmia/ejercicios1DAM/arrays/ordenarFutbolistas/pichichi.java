// Crear un array de objetos futbolista y ordenar el array ( con el método de la burbuja) para que éste sea la tabla de goleadores ordenados de mayor a menor número de goles.El programa acabará mostrando esa clasificación.
public class pichichi
{
	public static void main (String args[])
	{	
		System.out.println("¿Cuántos jugadores va a introducir?");
		int n = Integer.parseInt(System.console().readLine());

		futbolista jugador[]= crearFutbolistas(n);

		ordenarFutbolistas(jugador);

		mostrarFutbolistas(jugador);


	}
	public static futbolista[] crearFutbolistas(int numJugadores)
	{
		futbolista jugador[] = new futbolista[numJugadores];
		for(int i = 0; i<jugador.length; i++)
		{
			String nombre;
			int goles;
			System.out.println("Introduce el nombre del jugador: ");
			nombre= System.console().readLine();
			System.out.println("Introduce los goles que lleva anotados esta temporada: ");
			goles= Integer.parseInt(System.console().readLine());
			jugador[i]= new futbolista(nombre,goles);
		}
		return jugador;
	}
	public static futbolista[] ordenarFutbolistas(futbolista jugador[])
	{
		futbolista aux;
		for(int tope=jugador.length-1; tope>=0; tope--)
			for(int posicion=0;posicion<tope; posicion++)
				if(jugador[posicion].getGoles()< jugador[posicion+1].getGoles())
				{
					aux=jugador[posicion+1];
					jugador[posicion+1]= jugador[posicion];
					jugador[posicion]=aux;
				}
		return jugador;
	}
	public static void mostrarFutbolistas(futbolista jugador[])
	{
		System.out.println("Tabla de goleadores: ");
		for(int i=0; i<jugador.length; i++)
			System.out.println(jugador[i]);
	}
}