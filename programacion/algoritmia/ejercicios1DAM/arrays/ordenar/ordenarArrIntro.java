//Prgrama que pide un array y lo ordena.

public class ordenarArrIntro
{
	public static void main (String args[])
	{

		System.out.println("Introduce el número de datos que va a tener el array");
		int tam= Integer.parseInt(System.console().readLine());
		int numeros[] = new int[tam];

		//Pedir al usuario el array
		introduceArray(numeros);

		//Mostrar el array por pantalla
		muestraArray(numeros);

		
	}

	public static void introduceArray(int n[])
	{
		for(int i=0;i<n.length;i++)
		{
			System.out.println("Introduce el valor " + (i+1) + ": ");
			n[i]= Integer.parseInt(System.console().readLine());
		}

	}

	public static void muestraArray(int n[])
	{
		System.out.println("Los valores introducidos son: ");
		for(int i=0;i<n.length;i++)
		{
			System.out.print(n[i] + "\t");
		}
		System.out.println("");

	}
	public static void ordenaArray(int n[])
	{
		
	}
}