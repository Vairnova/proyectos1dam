/*Realiza un programa que, mediante la clase Hashtable, permita gestionar películas de una videoteca o videoclub. Para ello, se utilizarán entradas de tipo <Integer,String> tal que cada película tenga un número de película y su título.

	El programa permitirá:
	- introducir películas desde teclado
	- listar todas las películas
	- eliminar una película a partir de su número
	- consultar el título de una película a partir de su número
*/
import java.util.Hashtable;
public class hash
{
	 public static void main(String args[])
	{
		int num=0;
		String titulo;
		Hashtable<Integer,String> pelis = new Hashtable<Integer,String>();
		do
		{
			System.out.println("Introduce una película (Intro para terminar");
			titulo= System.console().readLine();
			num++;
			pelis.put(num,titulo);

		}while(!(titulo.equals("")));
		System.out.println("Introduce un número de pelicula");
		num= Integer.parseInt(System.console().readLine());
		titulo= pelis.get(num);
		System.out.println("Nº: " + num + " Título: " + titulo);
	}
	
}