//2. Escribir un programa que reciba como datos una cadena de caracteres y un carácter y reporta el número de veces que se encuentra el carácter en la cadena. 

public class ej2Strings
{
	public static void main (String args[])
	{
		String s1;
		String sCaracter;
		int contador = 0;

		System.out.println("Introduce palabras o texto");
		s1= System.console().readLine();

		System.out.println("Introduce una letra");
		sCaracter= System.console().readLine();
		char c = sCaracter.charAt(0);

		for(int i=0;i<s1.length(); i++)
			if (s1.charAt(i) == c)
				contador++;
		
		System.out.println("El carácter " + c + " está repetido " + contador + " veces en el texto introducido");
	}
}