// 4. Implemente un programa que reciba una cadena S y una letra X, y coloque en mayúsculas cada ocurrencia de X en S. (la función debe modificar la variable S).

public class ej4String
{
	public static void main (String args[])
	{
		char c;
		char cad[]=args[0].toCharArray();
		c= args[1].charAt(0);
		for(int i=0; i< cad.length; i++)
		{
			if(cad[i] == c)
				cad[i]= Character.toUpperCase(cad[i]);
		}
		System.out.println(cad);
	}
}