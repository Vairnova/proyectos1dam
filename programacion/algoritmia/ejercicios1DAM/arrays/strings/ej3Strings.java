//3. Implemente un programa que indique si una palabra es un palíndrome . Una palabra es palíndrome si se lee igual de izquierda a derecha que de derecha a izquierda. 

public class ej3Strings
{
	public static void main (String args[])
	{
	String palabra;
	String spalindrome;

	System.out.println("Introduce una palabra");
	palabra = System.console().readLine();	

	StringBuffer palindrome = new StringBuffer(palabra);

	palindrome.reverse();
	spalindrome= palindrome.toString();

	if( palabra.equals(spalindrome))
		System.out.println("La palabra introducida es un palíndrome");

	else
		System.out.println("La palabra introducida no es un palíndrome");

	}
}