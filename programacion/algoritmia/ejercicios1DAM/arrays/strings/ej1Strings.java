//1. Hacer un programa que ingrese una cadena de caracteres y determine el número de mayúsculas y el número de minúsculas.

public class ej1Strings
{
	public static void main (String args[])
	{
		String s;
		int minus=0, mayus=0;
		System.out.println("Introduce palabras");
		s= System.console().readLine();

		char cadena1[] = s.toCharArray();
		for (int i=0; i<cadena1.length; i++)
			if (cadena1[i] >64 && cadena1[i] <91)
			mayus++;
			else
				if(cadena1[i] >96 && cadena1[i] <123)
					minus++;
		
		System.out.println("El número de mayúsculas es: " + mayus + " y el número de minúsculas es: "+ minus);
	}
}