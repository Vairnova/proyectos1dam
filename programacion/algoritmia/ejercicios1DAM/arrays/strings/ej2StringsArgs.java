//2. Escribir un programa que reciba como datos una cadena de caracteres y un carácter y reporta el número de veces que se encuentra el carácter en la cadena. 

public class ej2StringsArgs
{
	public static void main (String args[])
	{
		
		int contador = 0;
		if(args.length != 2)
			System.out.println("Número de parámetros incorrecto, introduzca una palabra y una letra");
		else
		{
				char c= args[1].charAt(0);

				for(int i=0;i<args[0].length(); i++)
					if (args[0].charAt(i) == c)
						contador++;
		System.out.println("El carácter " + args[1] + " está repetido " + contador + " veces en el texto introducido");
		}
	}
}