//11. Programa que pide un número entero e imprime su tabla de multiplicar.
import java.util.Scanner;
public class enu11
{
	static void tabla(int numfunc)
	{
		for (int i = 0; i<=10; i++)
			System.out.println(numfunc + " x " + i + " = " + (numfunc*i));
	}	
	public static void main(String args[])
	{
		int num;
		Scanner ent = new Scanner(System.in);
		System.out.println("Introduce un número entero y se mostrará su tabla de multiplicar");
		num = ent.nextInt();
		System.out.println("La tabla del "+ num + " es: ");
		tabla(num);
		
	}
}