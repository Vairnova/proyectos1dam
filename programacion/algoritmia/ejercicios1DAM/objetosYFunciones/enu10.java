//10. Programa que muestra las 10 tablas de multiplicar (para 1,2,3,...,10).

public class enu10
{ 
	static void tablas(int n)
	{
		for(int i=0; i<=10;i++)									//Bucle interno que sumará 1 al contador hasta 10 y que multiplicará la tabla con el contador del bucle interno.
                System.out.println(n + " " + " x " + i + " = " + (i*n));
	}
    public static void main(String args[])
    {
        int tab = 0;
        for (; tab <= 10; tab++)		//Bucle externo que suma 1 al contador hasta 10 y que servirá para mostrar la siguiente tabla de multiplicar
        {
        	System.out.println("La tabla del " + tab + " es: \n"); //Indica la tabla que se va a mostrar.
            tablas (tab);
        	System.out.println("\n");  //retorno de carro después del bucle interno y antes de la repetición del externo para separar tablas
        }
    }
}
