/*Los números de Fibonacci son los miembros de una secuencia en la que cada numero es igual a la suma de los dos anteriores. En otras palabras, Fi = Fi-1 + Fi-2, donde Fi es el i-ésimo nº de Fibonacci, y siendo F1=F2=1. Por tanto:
	F5 = F4 + F3 = 3 + 2 = 5, y así sucesivamente.
	Escribir un programa que determine los n primeros números de Fibonacci, siendo n un valor introducido por teclado. */
// F3 = F2 + F1 --> F3 = 1 + 1 = 2    
// F4 = F3 + F2 --> F4 = 2 + 1 = 3
// F5 = F4 + F3 --> F5 = 3 + 2 = 5 
// F6 = F5 + F4 --> F6 = 5 + 3 = 8
// F7 = F6 + F5 --> F7 = 8 + 5 = 13
public class enu28
{
	public static void main (String args[])
	{
		int num, fibonacci = 1, aux1 = 1, prefibonacci = 1;
		System.out.println("Introduzca un número y se le mostrarán todos los números fibonacci que hay hasta él");
		num= Integer.parseInt(System.console().readLine());
		System.out.println("\nLos fibonacci entre 0 y " + num + " son: ");
		while (fibonacci <= num)
		{
			fibonacci= prefibonacci + aux1;
			aux1=prefibonacci;
			prefibonacci = fibonacci;
			
			if (fibonacci < num)
				System.out.println(fibonacci);
		}

	}
}