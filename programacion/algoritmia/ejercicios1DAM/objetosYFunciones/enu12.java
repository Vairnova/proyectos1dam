//12.  Programa que pide un número mayor que 0 al usuario y realiza comprobación del valor de manera que si el valor introducido no es correcto vuelva a solicitar la introducción del valor.
import java.util.Scanner;
public class enu12
{
    static double comprobar ()
    {
        double num;
        Scanner ent = new Scanner(System.in);
    do{
            System.out.println("Introduce un número positivo");
            num = ent.nextDouble();
          }while(num<0);
          return num;
      }
    public static void main(String args[])
    {
        double num;
        num = comprobar();
          System.out.println("Correcto, el número introducido es: " + num);
    }
}