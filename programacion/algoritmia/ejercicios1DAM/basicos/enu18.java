//18. Diseñar un programa que muestre la suma de los números impares comprendidos entre dos valores numéricos enteros y positivos introducidos por teclado.
/*Instrucciones: 
1. Si los valores están desordenados ordenarlos
2.Si n1 es par -> n1++
3.Si n1 es impar -> n1 = n1+2
3.For desde n1 hasta n2 con incremento 2
4.suma = suma + contador
*/
import java.util.Scanner;

public class enu18
{
	public static void main(String args[])
	{
		int num1, num2, aux, contador;
		Scanner ent = new Scanner(System.in);

		System.out.println("Introduzca dos números enteros");
		num1= ent.nextInt();
		num2= ent.nextInt();

		if (num1>num2)
			{
			aux = num1;
			num1 = num2;
			num2 = aux;
			}
			aux = num1;

		if (num1%2==0)
			num1++;
		else
			num1+=2;

		for(contador = 0; num1<num2;num1+=2)
			{
				contador= contador + num1;
			}
			System.out.println("La suma de los impares comprendidos entre " + aux + " y " + num2 + " es: " + contador);


	}
}