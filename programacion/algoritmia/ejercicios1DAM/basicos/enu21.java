//21. Programa que acepta números enteros mayores o iguales que 0 hasta acabar con un numero negativo e indique cuantos múltiplos de 2 se han introducido, cuantos  múltiplos de 3 y cuantos múltiplos de 2 y de 3 (de 6).
import java.util.Scanner;
public class enu21
{
	public static void main(String args[]) 
	{
		int num, mul2 = 0, mul3 = 0, mul23 = 0;
		Scanner ent = new Scanner(System.in);

		System.out.println("Introduzca un número entero positivo (negativo para terminar el programa)");
		num= ent.nextInt();

		while(num >= 0)
		{
			if (num%2 == 0 && num%3 == 0)
				mul23++;
			else
				if(num%2 == 0)
					mul2++;
				else
					if (num%3 == 0)
						mul3++;
					else
						System.out.println("No es múltiplo de 2 ni 3 ni ambos");

			System.out.println("Introduce el siguiente número(introduce un negativo para terminar)");
			num=ent.nextInt();
		}
		System.out.println("El número de múltiplos de 2 es: "+ mul2 + "\nEl número de múltiplos de 3 es: " + mul3 + "\nEl número de múltiplos de 2 y 3 al mismo tiempo es: " + mul23);

	}
}