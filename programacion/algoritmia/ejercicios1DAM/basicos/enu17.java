/*17.  Sustituir la siguiente estructura "para" por una de tipo "mientras":

	x=1
	y=1
	Para i desde 5 hasta 100 con incremento 5
		z=x+y
		x=y
		y=z
	Fpara
	*/

public class enu17
{
	public static void main(String args[])
	{
		int x = 1, y = 1, z, i=5;

		while(i<=100)
		{
			z = x+y;
			x = y;
			y = z;
			i+=5;
			System.out.println(z + " " + x + " " + y + " " + i);
		}
	}
}