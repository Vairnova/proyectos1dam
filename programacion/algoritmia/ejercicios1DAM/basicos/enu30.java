//30. Realizar un programa que lea números hasta que se dé un 0 y calcule la suma de sus dígitos.
public class enu30
{
	public static void main(String args[])
	{
		int num, sumdigito = 0;
		System.out.println("Introduce varios números y se contarán sus dígitos");
		num = Integer.parseInt(System.console().readLine());
		
		while (num != 0)
		{
			
			do
			{
				sumdigito=num%10 + sumdigito;
				num = num / 10;
			if (num/10 == 0)
				sumdigito = sumdigito + num%10;
			}while((num/10)>0);

		System.out.println("Introduzca el siguiente");
		num = Integer.parseInt(System.console().readLine());

		}

		System.out.println("La suma de todos los dígitos es: " + sumdigito);

	}
}