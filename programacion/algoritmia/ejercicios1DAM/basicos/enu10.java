//10. Programa que muestra las 10 tablas de multiplicar (para 1,2,3,...,10).

public class enu10
{
    public static void main(String args[])
    {int tabla = 0;
        for (; tabla <= 10; tabla++)		//Bucle externo que suma 1 al contador hasta 10 y que servirá para mostrar la siguiente tabla de multiplicar
        {
        	System.out.println("La tabla del " + tabla + " es: \n"); //Indica la tabla que se va a mostrar.
            for(int i=0; i<=10;i++)									//Bucle interno que sumará 1 al contador hasta 10 y que multiplicará la tabla con el contador del bucle interno.
                System.out.println(tabla + " " + " x " + i + " = " + (i*tabla)); 
        	System.out.println("\n");  //retorno de carro después del bucle interno y antes de la repetición del externo para separar tablas
        }
    }
}
