//4. Programa que pide un valor numérico e indica si éste es positivo o negativo.

import java.util.Scanner;
public class enu4
{
public static void main(String args[])
    {
        double num;
        Scanner ent = new Scanner (System.in);

        System.out.println("Introduce un número y se indicará si es positivo o negativo");
    
        num = ent.nextDouble();
        
        if (num < 0)
            System.out.println("El número es negativo");
        else
            System.out.println("El número es positivo");

    }
}
