//6. Programa que acepta 2 números y, si su suma es inferior a 10, pide un tercer valor. Al final muestra la suma de los valores introducidos.

import java.util.Scanner;
public class enu6
{
    public static void main  (String args[])
    {
        double num1, num2, num3;
        Scanner ent = new Scanner(System.in);
        
        System.out.println("Introduce dos números, se solicitará un tercero después si la suma de ambos es inferior a 10");
        num1= ent.nextDouble();
        num2= ent.nextDouble();
        
        if ((num1 + num2) < 10)
            {
            System.out.println("Introduzca un tercer número");
            num3= ent.nextDouble();
            System.out.println("El resultado de: " + num1 + " + " + num2 + " + " + num3 + " = " + (num1 + num2 + num3));
            }
        else
            System.out.println("El resultado de los dos números es: " + (num1 + num2));
    }
}

            
