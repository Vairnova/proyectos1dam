//27. Modificar el anterior algoritmo para que pida una lista de enteros acabada con un 0  e indique, para cada uno de ellos, si es primo o no. Además, al final ha de indicar cuántos primos se han introducido.
public class enu27
{
	 static boolean primo(int n)				//función que dirá si el número es primo o no
	{
		double resto = 0, aux = 0;

		for (int i = 2; i < n; i++)
		{
			resto = n % i;
			
			if(resto == 0)
				aux++;
		}

		if (aux > 0)
				return false;
			else
				return true;				
	}

	public static void main(String args[])
	{
		int num, cont = 0;
		boolean resultado;

		System.out.println("Introduzca varios números enteros positivos y se le indicará si son primos o no, para finalizar introduzca un 0 o un negativo");
		num = Integer.parseInt(System.console().readLine());
		while(num > 0)
		{
			
			
			resultado = primo(num);
			if (resultado== false)
				System.out.println("El número no es primo");
				
			else
			{
				System.out.println("El número es primo");
				cont++;
			}

			System.out.println("Introduzca varios números enteros positivos y se le indicará si son primos o no, para finalizar introduzca un 0 o un negativo");
			num = Integer.parseInt(System.console().readLine());
		}
		System.out.println("Se han introducido un total de " + cont + " números primos.");
	}

}