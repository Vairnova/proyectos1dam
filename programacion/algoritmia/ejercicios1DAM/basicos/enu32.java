/*32. Realizar un programa que lea un número del teclado y escriba en pantalla, en forma de cuadrado, la lista de números naturales desde el 1 hasta el número dado, rellenando los espacios sobrantes con 0.
	Ejemplo:
		Para el número 4:
				1 2 3 4
				2 3 4 0
				3 4 0 0
				4 0 0 0
*/

public class enu32
{
	public static void main (String args[])
	{
		int num, i, e, aux=0;
		System.out.println("Introduce un número y se dibujará en cuadrícula desde el 1 hasta éste");	
		num = Integer.parseInt(System.console().readLine());
		
		for (i= 1; i <= num; i++)
		{	

			

			
			for (e=i; e <= num; e++)
				{	
					System.out.print(e + " ");
					aux++;
				}

			for (; aux < num; aux++)
				if (num >=10)
					System.out.print(" 0 ");
				else
					System.out.print("0 ");
			
			aux = 0;
			System.out.print(" " + "\n");

		}	
	}
}
