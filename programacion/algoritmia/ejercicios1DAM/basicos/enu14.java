//14. Realizar el ordinograma y pseudocodigo de un programa que intercambie el valor de dos variables.

public class enu14
{
    public static void main(String args[])
    {
    	double num1,num2,aux;
    	num1 = 5;
    	num2 = 7;

    	System.out.println("Antes del intercambio " + num1 + " " + num2);
    	aux = num1;
    	num1 = num2;
    	num2 = aux;
    	System.out.println("Después del intercambio " +num1+ " " + num2);
    }
}