//2. Programa que pide 2 valores numéricos y muestra el resultado de su suma y su producto.

import java.util.Scanner;
public class enu2
{
    public static void main(String args[])
    {
        double num1,num2;
        Scanner ent = new Scanner (System.in);
        System.out.println("Introduce dos números que se sumarán y multiplicarán");
        
        num1= ent.nextDouble();
        num2= ent.nextDouble();
        
        System.out.println(num1 + " + " + num2 + " = "+ (num1 + num2));
        System.out.println(num1 + " x " + num2 + " = "+ (num1 * num2));
    }
}
