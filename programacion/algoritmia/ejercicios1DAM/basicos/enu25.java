//25. Escribir un programa que lea las temperaturas obtenidas en 15 observatorios meteorológicos y escriba la temperatura mínima y cuantas mínimas se han producido.

import java.util.Scanner;

public class enu25
{
	public static void main(String args[])
	{
		double temp, min = 0, cont = 0;
		Scanner ent = new Scanner(System.in);

		for( int i = 1; i<=15; i++)
		{
			System.out.println("Introduzca la temperatura del observatorio "+ i);
			temp = ent.nextDouble();

			if (i==1)
			{
				min = temp;
				cont++;
			}

			if(temp < min)
			{
				min = temp;
				cont++;
			}
		}
		System.out.println("La temperatura mínima recogida ha sido de: "+ min + "ºC\nLas veces que se ha batido el mínimo de temperatura ha sido de: "+ cont);
	}	
}