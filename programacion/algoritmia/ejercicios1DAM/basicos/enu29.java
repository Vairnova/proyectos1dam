//29.  Algoritmo que indique si un año, introducido por teclado, es bisiesto o no.
public class enu29
{
	public static void main(String args[])
	{
		int anyo;
		System.out.println("Introduzca un año para saber si es bisiesto");
		anyo = Integer.parseInt(System.console().readLine());

		if (anyo % 4 == 0)
			System.out.println("Es bisiesto");
		else
			System.out.println("No es bisiesto");
	}

}