//15. Realizar la siguiente traza:
/*INICIO
	Leer a
	c = 0
	Mientras c< 10
		c= c+2
		a= a +3
		Escribir a
	FinMientras
FIN
*/

import java.util.Scanner;
public class enu15
{
    public static void main(String args[])
    {
        double a,c;
        Scanner ent = new Scanner(System.in);
		System.out.println("Leer a");
		a= ent.nextDouble();
		c=0;
		while (c<10)
		{
			c = c + 2;				//a se incrementará durante varios ciclos del while hasta que c sea mayor que 10.
			a = a + 3;
			System.out.println(a);
		}
	}    
}
