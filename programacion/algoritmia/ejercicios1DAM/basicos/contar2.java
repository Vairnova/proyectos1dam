//Prgrama para contar el número de veces correctas que se ha introducido un número dentro de un rango y detenerse cuando se introduzca uno erróneo.

import java.util.Scanner;

public class contar2

{
    public static void main(String args[])
    {
        double n, cont=0;
        Scanner ent= new Scanner(System.in);
        do
        {
            System.out.println("Introduce un número entre 0 y 10");
            n= ent.nextDouble();
            if (n<=10 && n>=0)
                cont++;        
        }while (n<=10 && n>=0);
    System.out.println("Número erróneo, el total de números correctos introducido es de: " + cont);
    }
}
