//16. Programa que indique el máximo y el mínimo de un conjunto de notas introducidas desde teclado. La serie de notas acabará con un valor negativo.

import java.util.Scanner;
public class enu16
{
	public static void main(String args[])
	{
		double i,max,min;
		Scanner ent = new Scanner(System.in);
		System.out.println("Introduzca varias notas en número separadas por espacio o por intro (introduzca una negativa para finalizar)");
		i= ent.nextDouble();
		max = i;
		min = i;
		while (i>=0)
		{
			
			System.out.println("Introduzca la siguiente");
			i= ent.nextDouble();
			if(i>max)
				max = i;
			else
				if(i<min && i>=0)
				min = i;

		}
		System.out.println("El número máximo introducido es: " + max + "\nEl número mínimo introducido es: " + min);
		
		
	}
}
