//Programa para mostrar al usuario la calificación que obtiene según su nota. Ej:5- suficiente, 7-notable

import java.util.Scanner;

public class notas
{
    public static void main(String args[])
    {
        Scanner ent = new Scanner(System.in);
        double nota;
        
        System.out.println("Introduce tu nota en números");
        
        nota = ent.nextDouble(); //leerá el número que introduzca el usuario 
    
        if (nota < 5) 
            System.out.println ("Su nota " + nota + " equivale a un suspenso");  //si tiene menos de un 5 dirá suspenso
        else if(nota<6)
            System.out.println ("Su nota " + nota + " equivale a un suficiente"); //si tiene menos de un 6 dirá suficiente y no dice suspenso porque al estar anidado en el if anterior éste se encarga de aplicar el resultado a menor que 5 

            else if (nota < 7)
                System.out.println ("Su nota " + nota + " equivale a un bien");  //mismo procedimiento que el anterior pero esta vez con la nota 6

                else if (nota <9)
                    System.out.println ("Su nota " + nota + " equivale a un notable");

                    else if (nota < 10)
                        System.out.println("Su nota " + nota + " equivale a un sobresaliente");

                        else if (nota == 10)
                            System.out.println("Su nota " + nota+ " equivale a una matrícula de honor");

                        else
                            System.out.println("No es un número válido"); //Intentará negar que lo que se haya introducido no es válido si es mayor que 10. 


                        
            
    }
}
