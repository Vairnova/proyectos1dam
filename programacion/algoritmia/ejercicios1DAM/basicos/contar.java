// Programa que cuenta el número de veces que se ha introducido el número que pide correctamente hasta que se ha introducido uno erróneo.

import java.util.Scanner;

public class contar
{
    public static void main(String args[])
    {
        double num, cont=0;    //Añadimos una variable para el número que introduzca el usuario y otra para el contador, inicializamos el contador en 0 
        Scanner ent = new Scanner(System.in);
        
        System.out.println("Introduce un número entre 0 y 10");     //Le pedimos al usuario que introduzca un número
        
        num = ent.nextDouble();
        if (num <= 10 && num >= 0)                      //Comprobamos si está en el rango para hacer que el contador suba en 1
        cont++;

        while (num <= 10 && num >=0)                    //comenzamos un bucle que pida continuamente números entre 0 y 10 y cuando no esté en este rango saldrá
        {
            System.out.println("Correcto, introduce otro número");
            num = ent.nextDouble();

            if (num <= 10 && num >= 0)
            cont++;
        }
        System.out.println("Número erróneo, el total de números correctos introducido es de : " + cont);
    }
}
