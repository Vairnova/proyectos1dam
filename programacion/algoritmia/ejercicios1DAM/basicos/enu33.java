/* 33*. Realizar un programa que lea pares de números y escriba todos los números que están entre los números de cada par (sin incluirlos) y que NO sean ni el cuadrado ni el cubo de números entre el 1 y el 100.
	Se terminará el programa cuando alguno de los dos números leídos sea cero o negativo.
	Si no hay ningún número que cumpla la condición entre los dados se escribirá un 0.

	Ejemplo:
		Números leídos: 3 10, 48 50, 30 25, 0 8
		Resultado: 5 6 7, 0, 26 28 29, fin
*/
public class enu33
{
	public static void main(String args[])
	{
		int num1, num2, aux = 0, i = 0;
		
		System.out.println("Introduce dos números (negativo o 0 para salir del programa)");
		num1 = Integer.parseInt(System.console().readLine());
		num2 = Integer.parseInt(System.console().readLine());

		if(num1 > 0 || num2 > 0)		//solo entra si es mayor que 0 por lo que saldrá del programa en ese caso
		{
	
		if(num1>num2)				//intercambio de variables por si el usuario introduce los números desordenados
		{
			aux = num1;
			num1 = num2;
			num2 = aux;
			
			aux = 0;			//Ponemos la variable auxiliar a 0 para reutilizarla más adelante.
		}

		for(i = num1 + 1; i < num2; i++)  //igualamos i al número 1 para empezar a contar desde ahí y llegará hasta el valor de num2 incrementándose de uno en uno.
		{
			if (i < 100 && i > 1)  //nos aseguramos de que solo muestre si está entre el rango 1 - 100
				//if (i%2 != 0 && i % 3 != 0) 
				
			System.out.println(i);
			aux++;
		}		
		if (aux == 0)
			System.out.println("0");
			
										
		}	
	}
}
