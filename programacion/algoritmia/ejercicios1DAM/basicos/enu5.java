//5. Programa que, a partir de un entero introducido desde teclado, muestra todos los enteros, desde 1, menores que él.

import java.util.Scanner;
public class enu5
{
    public static void main  (String args[])
    {
        int num;
        Scanner ent = new Scanner(System.in);
        
        System.out.println("Introduce un número entero y se mostrarán todos los anteriores");
        num = ent.nextInt();
        
        System.out.println("Los números son: ");

        for (int i=0; i<num; i++)
            {
                System.out.println (i);
            }
    }
}
