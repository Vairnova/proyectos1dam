//24. Algoritmo que lea números desde teclado y finalice cuando se introduzca uno mayor que la suma de los dos anteriores. Escribir en pantalla el numero de valores introducidos y los valores que cumplieron la condición de finalización.

import java.util.Scanner;
public class enu24
{
	public static void main (String args[])
	{
		double num1, num2, suma = 0;
		int contador = 0;

		Scanner ent = new Scanner(System.in);
		
		System.out.println("Introduzca el primer número");
		num1 = ent.nextDouble();
		contador++;

		System.out.println("Introduzca el segundo número");
		num2 = ent.nextDouble();
		contador++;

		suma= num1+num2;
			
		while(suma > num1 || suma > num2)				//Hago repetición del código anterior porque si lo hiciera con un do while desde el principio no podría comprobar con los if si el número introducido es mayor que los anteriores porque suma habría que inicializarlo con 0 y siempre acabaría el bucle.
		{
			System.out.println("Introduzca el primer número");
			num1 = ent.nextDouble();
			contador++;

			if (num1>suma)
				break;	//Como ya está calculado desde el primer bloque del código uso break para que salga directamente del bucle en ambos casos.

			System.out.println("Introduzca el segundo número");
			num2 = ent.nextDouble();
			contador++;

			if (num2 > suma)
				break;

			suma = num1 + num2;
			
		}
		if(num1>suma)  //Para escribir un mensaje u otro y que quede más apropiado para cada situación en lugar de un mensaje general.
			System.out.println("Se han introducido un total de: " + contador + " valores.\n" + "El número que ha finalizado el proceso ha sido: " + num1 + " en cambio el segundo número ha quedado así: " + num2 + "\nLa suma que " + num1 + " ha superado ha sido: " + suma);
		else
			System.out.println("Se han introducido un total de: " + contador + " valores.\n" + "El número que ha finalizado el proceso ha sido: " + num2 + " en cambio el primer número ha quedado así: " + num1 + "\nLa suma que " + num2 + " ha superado ha sido: " + suma);
	}
}