//13. Realizar el programa que lea una cantidad positiva/ negativa de ºC y la pase a grados F.
import java.util.Scanner;
public class enu13
{
    public static void main(String args[])
    {
        double grados;
        Scanner ent = new Scanner(System.in);

        System.out.println("Introduce una temperatura en grados centígrados(negativa o positiva)");
        grados = ent.nextDouble();

        System.out.println("La conversión da: "+ ((grados*9/5)+32)+"ºF");
    }
}
