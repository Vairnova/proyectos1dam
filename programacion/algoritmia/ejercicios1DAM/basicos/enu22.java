//22. Algoritmo que solicita notas introducidas por teclado acabadas con un numero negativo, e imprime en pantalla el aprobado de nota más baja y el suspenso de nota más alta.

import java.util.Scanner;

public class enu22
{
	public static void main(String args[])
			{
				double nota, aprbaja = 10, susalta = 0;
				Scanner ent = new Scanner(System.in);

				System.out.println("Introduce una nota (negativa para finalizar)");
				nota = ent.nextDouble();

				while (nota >= 0)
				{
					if (nota < 5 && nota > susalta)
						susalta = nota;
					else
						if (nota >= 5 && nota < aprbaja)
							aprbaja = nota;

					System.out.println("Introduce la siguiente nota (negativa para finalizar)");
					nota = ent.nextDouble();
				}
				System.out.println("El aprobado más bajo es: " + aprbaja + "\nEl suspenso más alto es: " + susalta);
			}
}