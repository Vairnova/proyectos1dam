//19. Programa que acepte números positivos o iguales a 0 hasta acabar con un negativo y muestre por pantalla el total de valores validos introducidos.

import java.util.Scanner;

public class enu19
{
	public static void main (String args[])
	{
		double num;
		int cont=0;
		Scanner ent = new Scanner(System.in);

		System.out.println("Introduzca números positivos a contar (Introduzca un negativo para terminar)");
			num = ent.nextDouble();

			while (num >= 0)
			{
				cont++;
				System.out.println("Introduzca otro número positivo (negativo para terminar)");
				num = ent.nextDouble();
	
			}
		System.out.println("En total se ha introducido la cantidad de: "+ cont + " números mayores o iguales a 0.");
	
	}

}