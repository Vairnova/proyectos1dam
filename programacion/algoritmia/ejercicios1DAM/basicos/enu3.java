//3. Programa que acepta un número y muestra sus valores mitad y doble.

import java.util.Scanner;

public class enu3
{
public static void main(String args[])
    {
        double num;
        Scanner ent = new Scanner(System.in);
        
        System.out.println("Introduzca un número del cuál se mostrará su mitad y doble");
        num = ent.nextDouble();
        
        System.out.println("La mitad es: " + num/2 + "\n" + "El doble es: " + num*2);
 
    }
}
